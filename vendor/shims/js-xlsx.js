(function() {
	 /* globals define, XLSX */

  function vendorModule() {
    'use strict';

    var globalXLSX = self['XLSX'];

    return { 
			'default': XLSX,
			'CFB': XLSX.CFB, 
			'SSF': XLSX.SSF,
			'parse_xlscfb': XLSX.parse_xlscfb,
			'parse_zip': XLSX.parse_zip, 
			'read': XLSX.read, 
			'readFile': XLSX.readFile,
			'readFileSync': XLSX.readFileSync,
			'write': XLSX.write, 
			'writeFile': XLSX.writeFile,
			'writeFileSync' : XLSX.writeFileSync,
			'version': XLSX.version,
			'utils': XLSX.utils
		};
  }

  define('xlsx', [], vendorModule);
})();
